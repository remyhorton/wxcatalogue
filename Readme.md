wxCatalogue: Electronics catalogue tool
=======================================
_wxCatalogue_ is a little tool that was developed to allow me to
keep track of electronic components I had ordered in for various
projects, and in particular keep track of both how much I had in
stock as well as re-order codes. The program is written using
[wxPython][wxpy] and the background behind its development is
[covered elsewhere][tech]. The data is stored in a JSON file.

![Screenshot](screenshot.png)


## Current status
Writing _wxCatalogue_ solved the problem of keeping track of components,
although the approach taken was one of expedience as the development was
as much about getting back into wxPython as the underlying problem. The
major design decision was to store the data within the _TreeCtrl_ object
as it meant the position of items within the catalogue was handled for me
, but things like keeping information in sync between the _TreeCtrl_ and
the detail view shown by the _PropertyGrid_ threw up corner-cases.
I think the current version is at its architectural limit, so aside from
bug-fixes and maybe retrofitting some sort of basic search facility, I do
not expect to make any changes to the code-base.

_wxDatalogue_ is no longer maintained, the successor project being
[Leictreonach][leic] which is a complete rewrite using GTK.


## Release versions

### Version 1.0
I had not done any development since November 2018 so I declared the code
currently present to be `v1.0` - there are no changes other than adding
a Git tag.

### Version 1.1
This version is simply v1.0 converted to Python 3 and wxWidgets Phoenix.
The code-base used very few things that were removed for Python 3, and
logistically it was not before time to move away from Python 2.

### Version 2.x
Version 2.x is a rewrite using C++ and is on a [different branch][v2.x].

### Version 3.x
Renamed [Leictreonach][leic].


## Future plans
I plan to develop wxCatalogue further but the underlying design, which is
based around keeping the data within the TreeCtrl, is already at its limit
of what it can do. The main issue is the inability to have alternative
views without jumping through hoops, whereas for stock-checking purposes
at the very least I need a view that lists things based on storage location
rather than the main item classification. Therefore the next version will be
a ground-up reimplementation with a proper separation of the data from the
GUI, most likely using SQLite as a storage backend.

## Licence
_wxCatalogue_ is licenced under [version 3 of the GPL][gpl3].


## Contact
Send emails to ``remy.horton`` (at) ``gmail.com``

[wxpy]: https://wxpython.org/
[tech]: http://www.remy.org.uk/tech.php?tech=1539990000
[gpl3]: https://www.gnu.org/licenses/gpl.html
[v2.x]: https://bitbucket.org/remyhorton/wxcatalogue/src/v2.x/
[leic]: https://bitbucket.org/remyhorton/leictreonach