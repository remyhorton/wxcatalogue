#!/usr/bin/env python3
# wxCatalogue.py - Electronic component catalogue
#
# SPDX-License-Identifier: GPL-3.0-only
# (C) Remy Horton, 2018

import argparse
import wx
import wx.propgrid
import wx.grid
import json
import sys
import os


class Component(object):
    def __init__(self, dataInitial=None):
        if dataInitial is not None:
            self.title = dataInitial.get('title','New component')
            self.desc = dataInitial.get('desc','')
            self.code = dataInitial.get('code','')
            self.manuf = dataInitial.get('manuf','')
            self.vendors = dataInitial.get(
                'vendors',
                { 'Farnell' : '', 'Digikey' : ''}
                )
            self.notes = dataInitial.get('notes','')
            self.stock = dataInitial.get('stock',[])
        else:
            self.title = 'New component'
            self.desc = ''
            self.code = ''
            self.manuf = ''
            self.notes = ''
            self.vendors = {}
            self.stock = []

    def __str__(self):
        return "Component: {0} (Code:{1} {2} {3})".format(
            self.title, self.code,
            self.vendors.get('Farnell', ''),
            self.vendors.get('Digikey', '')
            )

    def toDict(self):
        return {
            'title' : self.title,
            'desc'  : self.desc,
            'code'  : self.code,
            'manuf' : self.manuf,
            'vendors' : self.vendors,
            'notes' : self.notes,
            'stock' : self.stock,
        }


class MainTree(wx.TreeCtrl):
    def __init__(self, parent):
        wx.TreeCtrl.__init__(self, parent, wx.NewId(),
            wx.DefaultPosition, wx.DefaultSize,
            wx.TR_HAS_BUTTONS | wx.TR_HIDE_ROOT | wx.TR_EDIT_LABELS)
        self.icons = wx.ImageList(16,16)
        self.iconFolder = self.icons.Add(
            wx.ArtProvider.GetBitmap( wx.ART_FOLDER, wx.ART_OTHER, (16,16) )
            )
        self.iconFolderOpen = self.icons.Add(
            wx.ArtProvider.GetBitmap( wx.ART_FILE_OPEN, wx.ART_OTHER, (16,16) )
            )
        self.iconItem = self.icons.Add(
            wx.ArtProvider.GetBitmap( wx.ART_NORMAL_FILE, wx.ART_OTHER, (16,16) )
            )

        self.SetImageList(self.icons)
        self.Bind(wx.EVT_TREE_BEGIN_DRAG, self.evtDrag)
        self.Bind(wx.EVT_TREE_END_DRAG, self.evtDragEnd)
        self.Bind(wx.EVT_TREE_SEL_CHANGED, self.evtSelChanged, self)
        #self.Bind(wx.EVT_TREE_ITEM_ACTIVATED, self.evtDblClick, self)
        self.Bind(wx.EVT_TREE_END_LABEL_EDIT, self.evtLabelChanged, self)
        self.Bind(wx.EVT_TREE_ITEM_MENU, self.evtItemMenu, self)

        self.idPopupMenu = [wx.NewId() for _ in range(0,6)]
        self.Bind(wx.EVT_MENU, self.evtPopupMenu0, id=self.idPopupMenu[0]);
        self.Bind(wx.EVT_MENU, self.evtPopupMenu1, id=self.idPopupMenu[1]);
        self.Bind(wx.EVT_MENU, self.evtPopupMenu2, id=self.idPopupMenu[2]);
        self.Bind(wx.EVT_MENU, self.evtPopupMenu3, id=self.idPopupMenu[3]);
        self.Bind(wx.EVT_MENU, self.evtPopupMenu4, id=self.idPopupMenu[4]);
        self.Bind(wx.EVT_MENU, self.evtPopupMenu5, id=self.idPopupMenu[5]);

        self.itemDrag = None
        self.unsaved = False

    def AddCategory(self):
        if not self.GetRootItem().IsOk():
            self.AddRoot("Components")
        itemSelected = self.GetSelection()
        if not itemSelected.IsOk():
            itemNew = self.AppendItem(self.GetRootItem(), "New category")
        else:
            if type(self.GetItemData(itemSelected)) is Component:
                itemTarget = self.GetItemParent(itemSelected)
                # Find last subcategory, if any
                itemChild,cookie = self.GetFirstChild(itemTarget)
                if self.isNotCategory(itemChild):
                # No subcateories
                    itemNew = self.PrependItem(itemTarget, "New subcategory")
                else:
                    # Find last category
                    while True:
                        itemNext,cookie = self.GetNextChild(
                            itemTarget,cookie=cookie)
                        if self.isNotCategory(itemNext):
                            # Last sub-category found
                            itemNew = self.InsertItem(
                                itemTarget,itemChild, "New Subcategory")
                            break
                    itemChild = itemNext
            elif itemSelected == self.GetRootItem():
                # Invisible root item selected
                itemNew = self.AppendItem(itemSelected, "New category")
            else:
                itemParent = self.GetItemParent(itemSelected)
                itemNew = self.InsertItem(
                itemParent, itemSelected, "New category")
        self.SetItemData(itemNew,None)
        self.SetItemImage(itemNew, self.iconFolder, wx.TreeItemIcon_Normal)
        self.SetItemImage(itemNew, self.iconFolderOpen, wx.TreeItemIcon_Expanded)
        self.SelectItem(itemNew)
        self.unsaved = True

    def AddComponent(self):
        if not self.GetRootItem().IsOk():
            self.AddRoot("Components")
        itemSelected = self.GetSelection()
        if not itemSelected.IsOk():
            return
        elif type(self.GetItemData(itemSelected)) is Component:
            itemTarget = self.GetItemParent(itemSelected)
        else:
            itemTarget = itemSelected
        component = Component()
        itemNew = self.AppendItem(itemTarget, component.title)
        component.menuItem = itemNew
        self.SetItemData(itemNew,component)
        self.SetItemImage(itemNew, self.iconItem, wx.TreeItemIcon_Normal)
        self.SelectItem(itemNew)
        self.unsaved = True

    def DeleteCurrent(self):
        itemSelected = self.GetSelection()
        if itemSelected.IsOk():
            self.Delete(itemSelected)
            self.unsaved = True

    def SaveBranch(self, itemRoot):
        listSubcats = []
        listComponents = []
        itemChild,cookie = self.GetFirstChild(itemRoot)
        while itemChild.IsOk():
            dataChild = self.GetItemData(itemChild)
            if type(dataChild) == Component:
                listComponents.append(dataChild)
            else:
                listSubcats.append( self.SaveBranch(itemChild) )
            itemChild,cookie = self.GetNextChild(itemRoot,cookie=cookie)
        return (self.GetItemText(itemRoot),listSubcats,listComponents)

    def RestoreBranch(self, itemRoot, data, itemTarget=None):
        (title,listSubcats,listComponents) = data
        if itemTarget is None:
            itemChild,cookie = self.GetFirstChild(itemRoot)
            if self.isNotCategory(itemChild):
                # No subcateories
                itemGrp = self.PrependItem(itemRoot, title)
            else:
                # Find last category
                while True:
                    itemNext,cookie = self.GetNextChild(itemRoot,cookie=cookie)
                    if self.isNotCategory(itemNext):
                        # Last sub-category found
                        itemGrp = self.InsertItem(itemRoot,itemChild,title)
                        break
                    itemChild = itemNext
        else:
            itemGrp = itemTarget

        self.SetItemData(itemGrp,None)
        self.SetItemImage(itemGrp, self.iconFolder, wx.TreeItemIcon_Normal)
        self.SetItemImage(itemGrp, self.iconFolderOpen, wx.TreeItemIcon_Expanded)
        for subcat in listSubcats:
            self.RestoreBranch(itemGrp, subcat)
        for component in listComponents:
            item = self.AppendItem(itemGrp, component.title)
            component.menuItem = item
            self.SetItemData(item,component)
            self.SetItemImage(item, self.iconItem, wx.TreeItemIcon_Normal)
        return itemGrp

    def FlattenSaveState(self, data):
        (title,listSubcats,listComponents) = data
        return (
            title,
            [self.FlattenSaveState(cat) for cat in listSubcats],
            [comp.toDict() for comp in listComponents]
            )

    def ExpandSaveState(self, data):
        (title,listSubcats,listComponents) = data
        return (
            title,
            [self.ExpandSaveState(cat) for cat in listSubcats],
            [Component(comp) for comp in listComponents]
            )

    def LoadData(self, strFilename):
        with open(strFilename,'r') as fp:
            self.DeleteAllItems()
            pancake = json.loads(fp.read())
            data = self.ExpandSaveState(pancake)
            itemRoot = self.AddRoot("Components")
            (_,listSubcats,listComponents) = data
            for subcat in listSubcats:
                self.RestoreBranch(itemRoot, subcat)
            for component in listComponents:
                item = self.AppendItem(itemRoot, component.title)
                component.menuItem = item
                self.SetItemData(item,component)
                self.SetItemImage(item, self.iconItem, wx.TreeItemIcon_Normal)
            self.unsaved = False

    def SaveData(self, strFilename):
        data = self.SaveBranch(self.GetRootItem())
        pancake = self.FlattenSaveState(data)
        strJSON = json.dumps(pancake)
        with file(strFilename,'w') as fp:
            fp.write(strJSON)
            self.unsaved = False

    def evtDrag(self, event):
        self.itemDrag = event.GetItem()
        event.Allow()

    def evtDragEnd(self, event):
        if not self.itemDrag:
            print("ERROR: Drag without source")
            return
        itemSource = self.itemDrag
        itemTarget = event.GetItem()
        if not itemTarget.IsOk():
            return
        if itemSource == itemTarget:
            return
        dataSource = self.GetItemData(itemSource)
        dataTarget = self.GetItemData(itemTarget)
        if type(dataSource) is Component:
            # Dragging component
            self.Delete(itemSource)
            if type(dataTarget) is Component:
                itemNew = self.InsertItem(
                    self.GetItemParent(itemTarget),
                    itemTarget, dataSource.title, self.iconItem, -1
                    )
            else:
                itemNew = self.AppendItem(
                    itemTarget,
                    dataSource.title, self.iconItem, -1)
            self.SetItemData(itemNew,dataSource)
            dataSource.menuItem = itemNew
            self.SelectItem(itemNew)
        else:
            # Dragging category folder
            dataCategory = self.SaveBranch(itemSource)
            strCategory = self.GetItemText(itemSource)
            self.Delete(itemSource)
            if type(dataTarget) is Component:
                itemNew = self.RestoreBranch(
                    self.GetItemParent(itemTarget), dataCategory)
            else:
                itemNew = self.RestoreBranch(itemTarget, dataCategory)
            self.SelectItem(itemNew)
        self.unsaved = True

    def evtSelChanged(self, event):
        item = event.GetItem()
        dataItem = self.GetItemData(item)
        self.detailMain.DisplayComponent( dataItem )
        event.Skip()

    def evtLabelChanged(self, event):
        item = event.GetItem()
        data = self.GetItemData(item)
        #event.Veto()
        if type(data) is Component:
            data.title = event.GetLabel()
            self.detailMain.DisplayComponent( data )
        self.unsaved = True

    def evtItemMenu(self, event):
        dataSelected = self.GetItemData(event.GetItem())
        if self.GetItemParent(event.GetItem()) == self.GetRootItem():
            notAtTop = True
        else:
            notAtTop = True
        menu = wx.Menu()
        menu.Append(self.idPopupMenu[3], "Add category")
        menu.Append(self.idPopupMenu[4], "Add component")
        menu.AppendSeparator()
        menu.Append(self.idPopupMenu[0], "Move up")
        menu.Append(self.idPopupMenu[1], "Mode into parent category"
            ).Enable(notAtTop)
        menu.Append(self.idPopupMenu[2], "Move down")
        menu.AppendSeparator()
        menu.Append(self.idPopupMenu[5], "Delete")
        self.PopupMenu(menu)
        menu.Destroy()

    def isComponent(self, item):
        if item.IsOk() and type(self.GetItemData(item)) is Component:
            return True
        return False

    def isNotCategory(self, item):
        if not item.IsOk():
            return True
        if type(self.GetItemData(item)) is Component:
            return True
        return False

    def evtPopupMenu0(self, event):
        # Move item up
        itemSource = self.GetSelection()
        dataSource = self.GetItemData(itemSource)
        if type(dataSource) == Component:
            # Component
            itemPrev = self.GetPrevSibling(itemSource)
            if not self.isComponent(itemPrev):
                return
            itemPrev = self.GetPrevSibling(itemPrev)
            if itemPrev.IsOk():
                itemNew = self.InsertItem(
                    self.GetItemParent(itemPrev), itemPrev,
                    dataSource.title, self.iconItem, -1
                    )
            else:
                itemNew = self.PrependItem(
                    self.GetItemParent(itemSource),
                    dataSource.title, self.iconItem, -1
                    )
            self.Delete(itemSource)
            self.SetItemData(itemNew,dataSource)
            dataSource.menuItem = itemNew
            self.SelectItem(itemNew)
            self.unsaved = True
        else:
            # Category
            itemPrev = self.GetPrevSibling(itemSource)
            if not itemPrev.IsOk():
                return
            itemRoot = self.GetItemParent(itemPrev)
            itemPrev = self.GetPrevSibling(itemPrev)
            dataCategory = self.SaveBranch(itemSource)
            strCategory = self.GetItemText(itemSource)
            self.Delete(itemSource)
            if not itemPrev.IsOk():
                itemGrp = self.PrependItem(itemRoot, dataCategory[0])
            else:
                itemGrp = self.InsertItem(itemRoot, itemPrev, dataCategory[0])
            self.RestoreBranch(itemRoot,dataCategory, itemGrp)
            self.SelectItem(itemGrp)
            self.unsaved = True

    def evtPopupMenu1(self, event):
        # Move item into parent
        itemSource = self.GetSelection()
        dataSource = self.GetItemData(itemSource)
        if type(dataSource) == Component:
            return
        itemParent = self.GetItemParent(itemSource)
        if itemParent == self.GetRootItem():
            return
        #itemPrev = self.GetPrevSibling(itemParent)
        itemRoot = self.GetItemParent(itemParent)
        dataCategory = self.SaveBranch(itemSource)
        strCategory = self.GetItemText(itemSource)
        self.Delete(itemSource)
        itemGrp = self.InsertItem(itemRoot, itemParent, dataCategory[0])
        self.RestoreBranch(itemRoot,dataCategory, itemGrp)
        self.SelectItem(itemGrp)
        self.unsaved = True

    def evtPopupMenu2(self, event):
        # Move item down
        itemSource = self.GetSelection()
        dataSource = self.GetItemData(itemSource)
        itemNext = self.GetNextSibling(itemSource)
        if not itemNext.IsOk():
            return
        if type(dataSource) == Component:
            # Component
            itemNew = self.InsertItem(
                self.GetItemParent(itemNext), itemNext,
                dataSource.title, self.iconItem, -1
                )
            self.Delete(itemSource)
            self.SetItemData(itemNew,dataSource)
            dataSource.menuItem = itemNew
            self.SelectItem(itemNew)
        else:
            # Category
            if type(self.GetItemData(itemNext)) == Component:
                return
            itemRoot = self.GetItemParent(itemNext)
            dataCategory = self.SaveBranch(itemSource)
            strCategory = self.GetItemText(itemSource)
            self.Delete(itemSource)
            itemGrp = self.InsertItem(itemRoot, itemNext, dataCategory[0])
            self.RestoreBranch(itemRoot,dataCategory, itemGrp)
            self.SelectItem(itemGrp)
        self.unsaved = True

    def evtPopupMenu3(self, event):
        self.AddCategory()

    def evtPopupMenu4(self, event):
        self.AddComponent()

    def evtPopupMenu5(self, event):
        self.DeleteCurrent()


class StockGrid(wx.grid.Grid):
    def __init__(self, parent, mainDetail):
        wx.grid.Grid.__init__(self, parent, -1)
        self.mainDetail = mainDetail
        self.SetRowLabelSize(0)
        self.CreateGrid(0, 2)
        self.SetColLabelValue(0, "Quantity")
        self.SetColLabelValue(1, "Location")
        self.SetColLabelAlignment(wx.ALIGN_LEFT, wx.ALIGN_CENTRE)
        self.DisableColResize(0)
        self.SetColSize(0,80)
        self.Bind(wx.EVT_SIZE, self.evtResize)
        self.Bind(wx.grid.EVT_GRID_CELL_CHANGED, self.evtChange)
        #self.Bind(wx.EVT_RIGHT_DOWN, self.evtRightClick)
        self.Bind(wx.grid.EVT_GRID_CELL_RIGHT_CLICK, self.evtRightClickRow)
        self.Bind(wx.grid.EVT_GRID_LABEL_RIGHT_CLICK, self.evtRightClickLabel)
        self.idPopupMenu = [wx.NewId() for _ in range(0,2)]
        self.Bind(wx.EVT_MENU, self.evtPopupMenu0, id=self.idPopupMenu[0]);
        self.Bind(wx.EVT_MENU, self.evtPopupMenu1, id=self.idPopupMenu[1]);

    def evtResize(self, event):
        self.SetColSize(1, event.GetSize().x - 80)
        #event.Skip()

    def evtChange(self, event):
        idxRow = event.GetRow()
        idxCol = event.GetCol()
        newValue = self.GetCellValue(idxRow,idxCol)
        self.mainDetail.ptrComponent.stock[idxRow][idxCol] = newValue
        self.mainDetail.treeMain.unsaved = True

    def evtRightClickLabel(self, event):
        menu = wx.Menu()
        menu.Append(self.idPopupMenu[0], "Add location")
        self.PopupMenu(menu)
        menu.Destroy()

    def evtRightClickRow(self, event):
        menu = wx.Menu()
        self.idxClickedRow = event.GetRow()
        menu.Append(self.idPopupMenu[0], "Add location")
        menu.Append(self.idPopupMenu[1], "Delete location")
        self.PopupMenu(menu)
        menu.Destroy()

    def evtPopupMenu0(self, event):
        self.AppendRows()
        self.mainDetail.ptrComponent.stock.append( ["",""] )

    def evtPopupMenu1(self, event):
        self.DeleteRows(pos=self.idxClickedRow)
        del self.mainDetail.ptrComponent.stock[self.idxClickedRow]


class MainDetail(wx.Panel):
    def __init__(self, parent):
        wx.Panel.__init__(self,parent)
        tabs = wx.Notebook(self, style=wx.BK_DEFAULT)

        self.gridProperties = wx.propgrid.PropertyGrid(tabs)

        prop = wx.propgrid.PropertyCategory("Item details")
        self.gridProperties.Append(prop)
        self.propTitle = wx.propgrid.StringProperty("Title")
        self.propDesc = wx.propgrid.StringProperty("Description")
        self.propManu = wx.propgrid.StringProperty("Manufacturer")
        self.propCode = wx.propgrid.StringProperty("Item code")
        self.gridProperties.Append(self.propTitle)
        self.gridProperties.Append(self.propManu)
        self.gridProperties.Append(self.propCode)
        self.gridProperties.Append(self.propDesc)
        prop = wx.propgrid.PropertyCategory("Vendor order codes")
        self.gridProperties.Append(prop)
        self.propFarnell = wx.propgrid.StringProperty("Farnell")
        self.propDigikey = wx.propgrid.StringProperty("Digi-Key")
        self.gridProperties.Append(self.propFarnell)
        self.gridProperties.Append(self.propDigikey)

        self.gridStock = StockGrid(tabs, self)

        self.textNotes = wx.TextCtrl(tabs, style=wx.TE_MULTILINE, id=wx.NewId())

        tabs.AddPage(self.gridProperties, "Details")
        tabs.AddPage(self.gridStock, "Stock")
        tabs.AddPage(self.textNotes, "Notes")

        sizer = wx.BoxSizer(wx.VERTICAL)
        sizer.Add( tabs, 1, wx.EXPAND, 0)
        self.SetSizer(sizer)
        #self.Layout()
        #print self.gridProperties.GetPropertyValues(inc_attributes=True)
        self.ShowDetails(False)

        self.Bind(wx.propgrid.EVT_PG_CHANGED, self.evtDetailsChanged)
        self.Bind(wx.EVT_TEXT, self.evtNotesChanged, id=self.textNotes.GetId())

    def DisplayComponent(self, component):
        if component is None:
            self.ShowDetails(False)
            self.propTitle.SetValue("")
            self.propDesc.SetValue("")
            self.propManu.SetValue("")
            self.propCode.SetValue("")
            self.propFarnell.SetValue("")
            self.propDigikey.SetValue("")
            self.textNotes.SetValue('')
            if self.gridStock.GetNumberRows() > 0:
                self.gridStock.DeleteRows(
                    numRows=self.gridStock.GetNumberRows())
        else:
            self.ptrComponent = component
            self.ShowDetails(True)
            self.propTitle.SetValue(component.title)
            self.propDesc.SetValue(component.desc)
            self.propManu.SetValue(component.manuf)
            self.propCode.SetValue(component.code)
            if "Farnell" in component.vendors.keys():
                self.propFarnell.SetValue(component.vendors["Farnell"])
            else:
                self.propFarnell.SetValue("")
            if "Digikey" in component.vendors.keys():
                self.propDigikey.SetValue(component.vendors["Digikey"])
            else:
                self.propDigikey.SetValue("")
            self.textNotes.SetValue(component.notes)
            if self.gridStock.GetNumberRows() > 0:
                self.gridStock.DeleteRows(
                    numRows=self.gridStock.GetNumberRows())
            for num,where in component.stock:
                self.gridStock.AppendRows()
                self.gridStock.SetCellValue(
                    self.gridStock.GetNumberRows()-1, 0, num)
                self.gridStock.SetCellValue(
                    self.gridStock.GetNumberRows()-1, 1, where)

    def ShowDetails(self,visible):
        self.Enable(visible)

    def evtDetailsChanged(self, event):
        prop = event.GetProperty()
        if prop.GetLabel() == "Title":
            self.treeMain.SetItemText(
                self.ptrComponent.menuItem, prop.GetValue())
        self.ptrComponent.title = self.propTitle.GetValue()
        self.ptrComponent.desc = self.propDesc.GetValue()
        self.ptrComponent.code = self.propCode.GetValue()
        self.ptrComponent.manuf = self.propManu.GetValue()
        self.ptrComponent.vendors["Farnell"] = self.propFarnell.GetValue()
        self.ptrComponent.vendors["Digikey"] = self.propDigikey.GetValue()
        self.treeMain.unsaved = True

    def evtNotesChanged(self, event):
        self.ptrComponent.notes = self.textNotes.GetValue()
        self.treeMain.unsaved = True


class StockApp(wx.App):
    def __init__(self, strDatafile):
        self.strDatabaseFilename = strDatafile
        wx.App.__init__(self)

    def OnInit(self):
        locale = wx.Locale();
        locale.Init(wx.LANGUAGE_DEFAULT, wx.LOCALE_LOAD_DEFAULT)

        self.frameMain = wx.Frame(None, id=wx.NewId())
        wx.BoxSizer(wx.HORIZONTAL)
        self.frameMain.SetInitialSize(wx.Size(640,480))

        menubar = wx.MenuBar()
        menuFile = wx.Menu()
        menuFile_AddCategory = menuFile.Append(
            wx.ID_ANY, "Add category",  "No help")
        menuFile_AddComponent = menuFile.Append(
            wx.ID_ANY, "Add component", "No help")
        menuFile_Del = menuFile.Append(
            wx.ID_ANY, "Delete", "")
        menuFile.AppendSeparator()
        menuFile_Load = menuFile.Append(
            wx.ID_ANY, "Load database", "")
        menuFile_Save = menuFile.Append(
            wx.ID_ANY, "Save database", "")
        menuFile_Import = menuFile.Append(
            wx.ID_ANY, "Import database", "")
        menuFile.AppendSeparator()
        menuFile_Exit = menuFile.Append(
            wx.ID_EXIT, "E&xit", "Terminate app")
        menubar.Append(menuFile, "&File")

        self.frameMain.Bind(
            wx.EVT_MENU, self.evtMenuAddCategory, menuFile_AddCategory)
        self.frameMain.Bind(
            wx.EVT_MENU, self.evtMenuAddComponent, menuFile_AddComponent)
        self.frameMain.Bind(wx.EVT_MENU, self.evtMenuLoad, menuFile_Load)
        self.frameMain.Bind(wx.EVT_MENU, self.evtMenuSave, menuFile_Save)
        self.frameMain.Bind(wx.EVT_MENU, self.evtMenuDel, menuFile_Del)
        self.frameMain.Bind(wx.EVT_MENU, self.evtMenuQuit, menuFile_Exit)
        self.frameMain.Bind(wx.EVT_MENU, self.evtMenuImport, menuFile_Import)
        self.frameMain.Bind(
            wx.EVT_CLOSE, self.evtClose, id=self.frameMain.GetId())
        splitterMain = wx.SplitterWindow(
            self.frameMain, wx.ID_ANY,
            style=wx.SP_LIVE_UPDATE|wx.SP_3D
            )
        self.tree = MainTree(splitterMain)
        self.detail = MainDetail(splitterMain)
        self.tree.detailMain = self.detail
        self.detail.treeMain = self.tree
        splitterMain.SetMinimumPaneSize(50)
        splitterMain.SplitVertically(self.tree, self.detail, 0)
        self.frameMain.SetTitle('Electronics catalogue')
        self.frameMain.SetMenuBar(menubar)
        self.frameMain.Show(True)
        if os.path.isfile(self.strDatabaseFilename):
            self.tree.LoadData(self.strDatabaseFilename)
        return True

    def evtMenuAddCategory(self, event):
        self.tree.AddCategory()

    def evtMenuAddComponent(self, event):
        self.tree.AddComponent()

    def evtMenuSave(self, event):
        if not self.tree.GetRootItem().IsOk():
                wx.MessageBox(
                    "No data to save!",
                    "Empty database",
                    wx.OK|wx.ICON_EXCLAMATION)
        else:
            self.tree.SaveData(self.strDatabaseFilename)

    def evtMenuLoad(self, event):
        if self.tree.unsaved:
            reply = wx.MessageBox(
                "There are unsaved changes. Revert to last saved version?",
                "Unsaved changes",
                wx.OK|wx.CANCEL|wx.CANCEL_DEFAULT
                )
        else:
            reply = wx.OK
        if reply == wx.OK:
            try:
                self.tree.LoadData(self.strDatabaseFilename)
            except IOError as ioe:
                wx.MessageBox(
                    ioe.strerror,
                    "Error loading {0}".format(self.strDatabaseFilename),
                    wx.OK|wx.ICON_EXCLAMATION
                    )

    def evtMenuDel(self, event):
        self.tree.DeleteCurrent()

    def evtMenuQuit(self, event):
        self.frameMain.Close()

    def evtMenuImport(self, event):
        dialogOpen = wx.FileDialog(
                self.frameMain, "Import catalogue",
                wildcard="Catalog files (*.json)|*.json",
                style=wx.FD_OPEN  | wx.FD_FILE_MUST_EXIST)
        if dialogOpen.ShowModal() == wx.ID_CANCEL:
            return
        with file(dialogOpen.GetPath(),'r') as fp:
            pancake = json.loads(fp.read())
            data = self.tree.ExpandSaveState(pancake)
            itemSubRoot = self.tree.AppendItem(
                    self.tree.GetRootItem(), "Imported components")
            self.tree.SetItemData(itemSubRoot,None)
            self.tree.SetItemImage(
                    itemSubRoot,
                    self.tree.iconFolder,
                    wx.TreeItemIcon_Normal)
            self.tree.SetItemImage(
                    itemSubRoot,
                    self.tree.iconFolderOpen,
                    wx.TreeItemIcon_Expanded)
            (_,listSubcats,listComponents) = data
            for subcat in listSubcats:
                self.tree.RestoreBranch(itemSubRoot, subcat)
            for component in listComponents:
                item = self.tree.AppendItem(itemSubRoot, component.title)
                component.menuItem = item
                self.tree.SetItemData(item,component)
                self.tree.SetItemImage(
                        item, self.tree.iconItem, wx.TreeItemIcon_Normal)
            self.tree.unsaved = True

    def evtClose(self, event):
        if event.CanVeto() and self.tree.unsaved:
            reply = wx.MessageBox(
                "Save changes before exiting?",
                "Save changes",
                wx.YES_NO|wx.CANCEL|wx.CANCEL_DEFAULT
                )
            if reply == wx.CANCEL:
                event.Veto()
                return
            elif reply == wx.YES:
                self.tree.SaveData(self.strDatabaseFilename)
        event.Skip()


class StockList(object):
    def __init__(self,strFilename):
        with file(strFilename,'r') as fp:
            pancake = json.loads(fp.read())
            self.listLeaves = self._traverse(pancake)
        self.dictCollections = dict()
        self.listDepleted = []
        for leaf in self.listLeaves:
            trail,item = leaf
            if item['stock'] == []:
                self.listDepleted.append(leaf)
            else:
                for line in [l[1] for l in item['stock']]:
                    try:
                        self.dictCollections[line].append(leaf)
                    except KeyError:
                        self.dictCollections[line] = [leaf]

    def _traverse(self,root,trail=None):
        (title,listSub,listComp) = root
        listLeaves = []
        if trail is None:
            for sub in listSub:
                listLeaves.extend( self._traverse(sub,[]) )
            return listLeaves
        newTrail = trail + [title]
        for sub in listSub:
            listLeaves.extend( self._traverse(sub,newTrail) )
        for item in listComp:
            listLeaves.append( (newTrail, item) )
        return listLeaves

    def getLocations(self):
        setLocs = set()
        for listLines in [leaf[1]['stock'] for leaf in self.listLeaves]:
            if listLines != []:
                for loc in [line[1] for line in listLines]:
                    setLocs.add(loc)
        return list(setLocs)

    def getInStock(self):
        listStores = []
        for loc,val in self.dictCollections.iteritems():
            listItems = []
            for item in self.dictCollections[loc]:
                listItems.append( '/'.join(item[0])+' '+item[1]['title'] )
            listStores.append( (loc,listItems) )
        return listStores


if __name__ == "__main__":
    strDatabase = 'catalogue.json'
    argvParser = argparse.ArgumentParser(
        description='Electronics catalogue',
        epilog=''
        )
    argvParser.add_argument('--categories', action='store_true',
        help='Show categories')
    argvParser.add_argument('--stocked', action='store_true',
        help='Show component stocks')
    argvParser.add_argument('--depleted', action='store_true',
        help='Show component stocks')
    argvParser.add_argument('--database',
        help='Database file (defaults to {0}'.format(strDatabase),
        metavar='file.json', type=str, nargs=1, # nargs=int,'?','+'
        )
    args = argvParser.parse_args(sys.argv[1:])

    if args.database is not None:
        print("Using '{0}' for database..".format(args.database))
        strDatabase = args.database[0]

    if args.stocked or args.depleted or args.categories:
        stock = StockList(strDatabase)
        if args.categories:
            for location,_ in stock.getInStock():
                print(location)
        if args.stocked:
            print()
            print("Current stock")
            print("=============")
            print()
            for location,listItems in stock.getInStock():
                print(location)
                for item in listItems:
                    print("   ",item)
        if args.depleted:
            print()
            print("Items out of stock")
            print("==================")
            print()
            for trail,item in stock.listDepleted:
                print("    ",item['title'])
    else:
        app = StockApp(strDatabase)
        app.MainLoop()
